package net.nilosplace.process_display.util;

import java.io.*;
import java.util.List;

import net.nilosplace.process_display.util.parallel.*;

public class ObjectFileStorage<T> {

	public void writeObjectToFile(T object, String filePath) throws Exception {
		writeObjectToFile(object, new File(filePath));
	}
	
	public void writeObjectToFile(T object, File file) throws Exception {
		FileOutputStream fos = new FileOutputStream(file);
		ParallelGZIPOutputStream pgzos = new ParallelGZIPOutputStream(fos);
		ObjectOutputStream oos = new ObjectOutputStream(pgzos);
		oos.writeObject(object);
		oos.flush();
		oos.close();
	}
	
	public void writeObjectsToFile(List<T> objects, String filePath) throws Exception {
		writeObjectsToFile(objects, new File(filePath));
	}
	
	public void writeObjectsToFile(List<T> objects, File file) throws Exception {
		FileOutputStream fos = new FileOutputStream(file);
		ParallelGZIPOutputStream pgzos = new ParallelGZIPOutputStream(fos);
		ObjectOutputStream oos = new ObjectOutputStream(pgzos);
		oos.writeObject(objects);
		oos.flush();
		oos.close();
	}
	
	public T readObjectFromFile(String filePath) throws Exception {
		return readObjectFromFile(new File(filePath));
	}
	
	public T readObjectFromFile(File file) throws Exception {
		FileInputStream fis = new FileInputStream(file);
      ParallelGZIPInputStream gzis = new ParallelGZIPInputStream(fis);
      ObjectInputStream in = new ObjectInputStream(gzis);
      T dataObject = (T)in.readObject();
      in.close();
      return dataObject;
	}
	
	public List<T> readObjectsFromFile(String filePath) throws Exception {
		return readObjectsFromFile(new File(filePath));
	}
	
	public List<T> readObjectsFromFile(File file) throws Exception {
		FileInputStream fis = new FileInputStream(file);
      ParallelGZIPInputStream gzis = new ParallelGZIPInputStream(fis);
      ObjectInputStream in = new ObjectInputStream(gzis);
      List<T> dataObjects = (List<T>)in.readObject();
      in.close();
      return dataObjects;
	}
	
}
