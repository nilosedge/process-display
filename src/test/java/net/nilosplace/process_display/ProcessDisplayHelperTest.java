package net.nilosplace.process_display;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class ProcessDisplayHelperTest extends TestCase {

	public ProcessDisplayHelperTest( String testName ) {
		super( testName );
	}

	public static Test suite() {
		return new TestSuite( ProcessDisplayHelperTest.class );
	}

	public void testApp() {
		ProcessDisplayHelper ph = new ProcessDisplayHelper(1);
		ph.startProcess("TPDH", 1000000);
		for(int i = 0; i < 1000000; i++) {
			ph.progressProcess();
		}
		ph.finishProcess();
		assertTrue( true );
	}
}
